module Doboz
  class Code

    def self.charge(charge_params)
      Doboz::Transaction.charge_code(charge_params, false)
    end

    def self.simulate_charge(charge_params)
      params_for_simulation = Doboz::Validator.set_nsf_for_simulate!(charge_params)
      Doboz::Transaction.charge_code(params_for_simulation, true)
    end

    def self.get_maximum_value(code)
      code_details = self.get_details(code)
      maximum_value = 0
      code_details['valueStores'].each do |valueStore|
        if valueStore['state'] == 'ACTIVE'
          maximum_value += valueStore['value']
        end
      end
      maximum_value
    end

    def self.get_details(code)
      response = Doboz::Connection.make_get_request_and_parse_response("giftcards/detail?card_number=#{CGI::escape(code)}")
      response['data']
    end

  end
end