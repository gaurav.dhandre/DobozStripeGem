module Doboz
  class ShopperTokenFactory
    def self.generate (contact, validity_in_seconds=nil)
      raise Doboz::BadParameterError.new("Doboz::api_key is not set") unless Doboz::api_key
      raise Doboz::BadParameterError.new("Doboz::shared_secret is not set") unless Doboz::shared_secret

      raise Doboz::BadParameterError.new("Must provide a contact with one of shopper_id, contact_id or user_supplied_id to generate a shopper token") unless (Doboz::Validator.has_valid_or_empty_shopper_id?(contact) ||
          Doboz::Validator.has_valid_contact_id?(contact) ||
          Doboz::Validator.has_valid_user_supplied_id?(contact))

      g = {}
      if (Doboz::Validator.has_valid_or_empty_shopper_id?(contact))
        g['shi'] = Doboz::Validator.get_shopper_id(contact)
      elsif (Doboz::Validator.has_valid_contact_id?(contact))
        g['coi'] = Doboz::Validator.get_contact_id(contact)
      elsif (Doboz::Validator.has_valid_user_supplied_id?(contact))
        g['cui'] = Doboz::Validator.get_user_supplied_id(contact)
      end


      payload = Doboz::api_key.split('.')
      payload = JSON.parse(Base64.decode64(payload[1]))

      g['gui'] = payload['g']['gui']
      g['gmi'] = payload['g']['gmi']

      iat = Time.now.to_i
      payload = {
          g: g,
          iat: iat,
          iss: "MERCHANT"
      }

      if validity_in_seconds
        payload['exp'] = iat + validity_in_seconds
      end

      JWT.encode(payload, Doboz::shared_secret, 'HS256')
    end
  end
end