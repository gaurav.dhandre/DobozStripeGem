module Doboz
  class Connection
    def self.connection
      conn = Faraday.new Doboz.api_base, ssl: {version: :TLSv1_2}
      conn.headers['Content-Type'] = 'application/json; charset=utf-8'
      conn.headers['Authorization'] = "Bearer #{Doboz.api_key}"
      conn.headers['User-Agent'] = "Doboz-Ruby/#{Gem.loaded_specs['doboz_client'].version.version}"
      conn
    end

    def self.ping
      self.make_get_request_and_parse_response('ping')
    end

    def self.make_post_request_and_parse_response (url, body)
      resp = Doboz::Connection.connection.post do |req|
        req.url url
        req.body = JSON.generate(body)
      end
      self.handle_response(resp)
    end

    def self.make_get_request_and_parse_response (url)
      resp = Doboz::Connection.connection.get {|req| req.url url}
      return  self.handle_response(resp)
    end

    def self.handle_response(response)
      body = JSON.parse(response.body) || {}
      message = body['message'] || ''
      case body['code']
        when 200...300
          return JSON.parse(response.body)
        when 400
          if (message =~ /insufficient value/i)
            raise Doboz::InsufficientValueError.new(message, response)
          else
            raise Doboz::BadParameterError.new(message, response)
          end
        when 401, 403
          raise Doboz::AuthorizationError.new(message, response)
        when 404
          raise Doboz::CouldNotFindObjectError.new(message, response)
        when 409
          raise Doboz::BadParameterError.new(message, response)
        else
          raise DobozError.new("Server responded with: (#{response.status}) #{message}", response)
      end
    end
  end
end
