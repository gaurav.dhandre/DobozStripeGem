module Doboz
  class Ping < Doboz::DobozObject
    attr_accessor :username, :mode, :scopes, :roles, :effectiveScopes

    def self.ping
      response = Doboz::Connection.ping
      Doboz::Validator.validate_ping_response!(response)
      self.new(response['user'])
    end
  end
end