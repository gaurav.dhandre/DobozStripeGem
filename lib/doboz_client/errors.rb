module Doboz
  class DobozError < StandardError
    attr_reader :message
    attr_accessor :response

    def initialize (message='', response)
      @message = message
      @response = response
    end
  end

  class AuthorizationError < DobozError
  end

  class InsufficientValueError < DobozError
  end

  class BadParameterError < DobozError
  end

  class CouldNotFindObjectError < DobozError
  end

  class IdempotencyError < DobozError
  end

  class ThirdPartyPaymentError < DobozError
  end


  class DobozArgumentError < ArgumentError
  end

end