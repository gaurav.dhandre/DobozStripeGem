require "faraday"
require "openssl"
require "json"
require "securerandom"
require "jwt"
require "base64"

require "doboz_client/version"

require "doboz_client/constants"
require "doboz_client/errors"
require "doboz_client/validator"
require "doboz_client/connection"
require "doboz_client/shopper_token_factory"

require "doboz_client/doboz_object"
require "doboz_client/ping"
require "doboz_client/transaction"
require "doboz_client/card"
require "doboz_client/code"
require "doboz_client/contact"
require "doboz_client/account"

module Doboz
  class << self
    attr_accessor :api_base, :api_key, :shared_secret
  end
  @api_base = 'https://localhost:3000'
end
